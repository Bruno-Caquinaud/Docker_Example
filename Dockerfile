#Docker File example to install opencv and tesseract lib

FROM ubuntu

ENV TZ="Europe/London"

RUN apt-get update -y
RUN apt-get install g++ -y
RUN apt-get install cmake -y
RUN apt-get install git -y
RUN apt-get install libx11-dev -y
RUN apt-get install libxtst-dev -y
RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install tzdata
RUN apt-get install tesseract-ocr -y
RUN apt-get install libtesseract-dev -y
RUN apt-get install libleptonica-dev -y

RUN mkdir -p /home/bruno/lib
WORKDIR /home/bruno/lib
RUN git clone https://github.com/opencv/opencv.git
RUN git clone https://github.com/opencv/opencv_contrib.git
RUN mkdir -p opencv/build
WORKDIR opencv/build
RUN cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_EXAMPLES:BOOL=OFF -DBUILD_PERF_TESTS:BOOL=OFF -DBUILD_TESTS:BOOL=OFF -DBUILD_DOCS:BOOL=OFF -DBUILD_LIST=core,highgui,imgproc,imgcodecs -DOPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules/text ../
RUN make -j4
RUN make install
RUN sed -i 's/COUNT/COUNT1/g' /usr/local/include/opencv4/opencv2/core/types.hpp

WORKDIR /home/bruno
RUN rm -rf *
